use std::fs::File;
use std::path::Path;
use std::io::Read;
use std::fs::OpenOptions;
use std::io::Write;

#[derive(Default)]
struct Persona{
    nombre: String,
    edad: u8,
    rut: String,
    mascota: [Mascota; 5]
}

#[derive(Default)]
struct Mascota{
    nomb: String,
    tipo: String,
    color: String,
}


fn read_file(mut f: &File){
    let mut text = String::new();
    f.read_to_string(&mut text).unwrap();
    println!("{}", &text);


}


fn create_blank_file(p: &Path){
    let file = File::create(p).expect("El archivo no pudo crearse");
    println!("El archivo fue creado");

}


fn add_new_content(mut f: &File){

    f.write_all(b"Nuevo texto\n");
}


fn open_file_to_append(p: &Path) -> File{

    let mut binding = OpenOptions::new();
    let binding = binding.append(true);
    let file = match binding.open(p){
        Err(why) => panic!("No se puede abrir el archivo"),
        Ok(file) => file,
    };

    //add_new_content(&file);

    return file
}


fn open_file_to_read(p: &Path){
    if Path::new(p).exists(){
        let file = match File::open(&p){
            Err(why) => panic!("El archivo no se puede abrir..."),
            Ok(file) => file,
        };
        read_file(&file)
    } else {
        create_blank_file(p);
    }
}

//Dentro de esta función se pueden agregar datos y cada vez que se ejecute
//se añadirán al txt

fn create_persona(p: &Path){

    let mut m1 = Mascota{nomb:"Bad Bunny".to_string(),
                         tipo:"Perro".to_string(),
                         color:"Blanco".to_string()};


    let mut p1 = Persona{nombre:"Benjamín".to_string(),
                             edad: 19,
                             rut: "21250500-5".to_string(),
                             mascota: Default::default()};
    
    p1.mascota[0] = m1;

    let temp = format!("{} : {} : {} : {} : {} : {}\n", p1.nombre, 
                                              p1.edad, 
                                              p1.rut, 
                                              p1.mascota[0].nomb,
                                              p1.mascota[0].tipo,
                                              p1.mascota[0].color
                                              );
    

    let mut file = open_file_to_append(p);

    file.write_all(temp.as_bytes());
    
}

fn main(){
    let path = Path::new("./ejemplo.txt");
    open_file_to_read(path);
    create_persona(path);
    open_file_to_read(path);
}
